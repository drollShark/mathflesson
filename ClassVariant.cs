﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CubLesson
{
    namespace ClassVariant
    {
        public abstract class Polygon
        {
            public void Perimeter() { }

            public int Perimeter(int a, int b, int c, int d)
            {
                return a + b + c + d;
            }
            public abstract int Area(int a);
            public abstract int Area(int a, int b);
        }


        public class Quad : Polygon
        {
            public override int Area(int a)
            {
                return (int)Math.Pow(a, 2);
            }

            public override int Area(int a, int b)
            {
                return 0;
            }
        }


        public class Rectangle : Polygon
        {
            public override int Area(int a, int b)
            {
                return a * b;
            }

            public override int Area(int a)
            {
                return 0;
            }
        }


        public class Rhombus : Polygon
        {

            public override int Area(int a)
            {
                return (int)(MathF.Pow(a, 2) * Math.Sin(a));
            }

            public override int Area(int a, int b)
            {
                return 0;
            }
        }
    }
   
}


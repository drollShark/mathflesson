﻿using System;

namespace CubLesson
{
    namespace ClassVariant
    {
        class Program
        {
            static void Main(string[] args)
            {
                Quad quad = new Quad();

                Console.WriteLine($"Периметр{quad.Perimeter(1, 1, 1, 1)} \n" + $"Площадь:{quad.Area(1)}");

                Rectangle rectangle = new Rectangle();

                Console.WriteLine($"Периметр{rectangle.Perimeter(2, 2, 4, 4)} \n" + $"Площадь:{rectangle.Area(2, 4)}");

                Rhombus rhombus = new Rhombus();

                Console.WriteLine($"Периметр{rhombus.Perimeter(4, 4, 2, 2)} \n" + $"Площадь:{rhombus.Area(2)}");

                Console.ReadLine();
            }

        }
    }

    namespace InterfaceVariant
    {
        class Program
        {
            static void Main(string[] args)
            {
                Quad quad = new Quad();

                quad.Area(1);
                quad.Perimeter(1, 1, 1, 1);
            }

        }

        abstract class PolygonPerimetr
        {
            public void Perimeter() { }

            public int Perimeter(int a, int b, int c, int d)
            {
                return a + b + c + d;
            }
        }

        class Quad : PolygonPerimetr, IPolygonArea
        {
            public int Area(int a)
            {
                return (int)Math.Pow(a, 2);
            }

            public int Area(int a, int b)
            {
                return 0;
            }
        }

        class RecRectangle : PolygonPerimetr, IPolygonArea
        {
            public int Area(int a)
            {
                return 0;
            }

            public int Area(int a, int b)
            {
                return a * b;
            }
        }

        class Rhombus : PolygonPerimetr, IPolygonArea
        {
            public int Area(int a)
            {
                return (int)(MathF.Pow(a, 2) * Math.Sin(a));
            }

            public int Area(int a, int b)
            {
                return 0;
            }
        }



    }
}

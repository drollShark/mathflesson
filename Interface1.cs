﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CubLesson
{
    interface IPolygonArea
    {
        public int Area(int a);
        public int Area(int a, int b);
    }
}
